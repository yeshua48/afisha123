import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
/* angular-cli file: src/styles.css */


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
