import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistavkiComponent } from './vistavki.component';

describe('VistavkiComponent', () => {
  let component: VistavkiComponent;
  let fixture: ComponentFixture<VistavkiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistavkiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistavkiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
