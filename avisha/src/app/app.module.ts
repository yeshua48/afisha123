import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import {Routes, RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlatpickrModule } from 'angularx-flatpickr';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { KinoComponent } from './kino/kino.component';
import { TeatrComponent } from './teatr/teatr.component';
import { VistavkiComponent } from './vistavki/vistavki.component';
import { KoncertiComponent } from './koncerti/koncerti.component';
import { HomeComponent } from './home/home.component';

const appRoutes: Routes = [
  {path: `kino`, component: KinoComponent },
  {path: `teatr`, component: TeatrComponent},
  {path: `vistavki`, component: VistavkiComponent},
  {path: `koncerti`, component: KoncertiComponent},
  {path: ``, component: HomeComponent},
];
@NgModule({
  declarations: [
    AppComponent,
    KinoComponent,
    TeatrComponent,
    VistavkiComponent,
    KoncertiComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {

}

