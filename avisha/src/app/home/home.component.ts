import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  viewDate = new Date();
  events = [
    {
      start: new Date('2019-10-03'),
      end: new Date('2019-10-16'),
      title: 'КиноРелизы',
      color: 'red',
    },
    ];


    constructor() {
    }

  ngOnInit() {
  }

}
